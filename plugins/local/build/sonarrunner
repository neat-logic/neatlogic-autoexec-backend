#!/usr/bin/perl
use strict;
use FindBin;
use Fcntl qw(:flock);
use File::Basename;
use Getopt::Long;
use File::Path;
use IO::File;
use File::Temp;

use DeployUtils;
use ServerAdapter;
use SonarQubeAdapter;

sub usage {
    my $pname = $FindBin::Script;

    print("Usage: $pname [--envpath EnvPath] [--version VERSION] [--language ProgrammingLanguage]\n");
    print("              [--baseurl APIAddress] [--username UserName] [--password Password]\n");
    print("              [--prefix SubDirOfProject]  [--encode SourceFileEncoding]  [--level Level]\n");
    print("              [--threshold ThresHold] [--modules <module1,module2>]\n");
    print("       --envpath:  subsys env path in the data directory, example:ATM/ATMP/PRD \n");
    print("       --version:   versoin name\n");
    print("       --buildno:   Build number\n");
    print("       --baseurl:   SonarQube API address\n");
    print("       --prefix:    sub directory of project\n");
    print("       --encode:    project encode,example:UTF-8 \n");
    print("       --language:  project develop language,example:java \n");
    print("       --level:     warning level ,example:blocker,critical \n");
    print("       --threshold: waning threshold(number),example:50,100.. \n");
    print("       --modules:   modules\n");
    exit(1);
}

sub main {
    my ( $isHelp,   $verbose,    $envPath, $version, $buildNo );
    my ( $sonarUrl, $sonarLogin, $sonarPwd );
    my ( $prefix,   $language,   $encode, $level, $threshold, $src, $bin, $timeout, $projectKey, $projectVersion, $projectName, $modules, $intervalDay );
    my $pname = $FindBin::Script;

    GetOptions(
        'h|help'           => \$isHelp,
        'envpath=s'        => \$envPath,
        'version=s'        => \$version,
        'buildno=s'        => \$buildNo,
        'baseurl=s'        => \$sonarUrl,
        'user=s'           => \$sonarLogin,
        'password=s'       => \$sonarPwd,
        'prefix=s'         => \$prefix,
        'language=s'       => \$language,
        'encode=s'         => \$encode,
        'level=s'          => \$level,
        'threshold=i'      => \$threshold,
        'src=s'            => \$src,
        'bin=s'            => \$bin,
        'timeout=s'        => \$timeout,
        'projectKey=s'     => \$projectKey,
        'projectVersion=s' => \$projectVersion,
        'projectName=s'    => \$projectName,
        'modules=s'        => \$modules,
        'intervalDay=i'    => \$intervalDay,
        'verbose=i'        => \$verbose
    );

    usage() if ( defined($isHelp) );

    my $deployUtils = DeployUtils->new();
    my $buildEnv    = $deployUtils->deployInit( $envPath, $version, $buildNo );

    $envPath = $buildEnv->{NAME_PATH};
    $version = $buildEnv->{VERSION};
    $buildNo = $buildEnv->{BUILD_NO};

    my $optionError = 0;
    if ( not defined($envPath) or $envPath eq '' ) {
        $optionError = 1;
        print("ERROR: EnvPath not defined by option --envpath or Environment:NAME_PATH\n");
    }
    if ( not defined($version) or $version eq '' ) {
        $optionError = 1;
        print("ERROR: Version not defined by option --version or Environment:VERSION\n");
    }
    if ( not defined($buildNo) or $buildNo eq '' ) {
        $optionError = 1;
        print("ERROR: Build number not defined by option --buildno or Environment:BUILD_NO\n");
    }
    if ( $optionError == 1 ) {
        usage();
    }

    # my $lock        = DeployLock->new($buildEnv);
    # my $spaceLockId = $lock->lockWorkspace($DeployLock::READ);

    # END {
    #     local $?;
    #     if ( defined($lock) ) {
    #         $lock->unlockWorkspace($spaceLockId);
    #     }
    # }

    my $toolsPath = $buildEnv->{TOOLS_PATH};

    my $prjDir     = $buildEnv->{PRJ_PATH};
    my $sysName    = $buildEnv->{SYS_NAME};
    my $moduleName = $buildEnv->{MODULE_NAME};

    $src    = $prjDir if ( not defined($src)    or $src eq '' );
    $bin    = $prjDir if ( not defined($bin)    or $bin eq '' );
    $encode = 'UTF-8' if ( not defined($encode) or $encode eq '' );

    $projectName    = $moduleName            if ( not defined($projectName)    or $projectName eq '' );
    $projectKey     = "$sysName.$moduleName" if ( not defined($projectKey)     or $projectKey eq '' );
    $projectVersion = $version               if ( not defined($projectVersion) or $projectVersion eq '' );

    if ( defined($prefix) ) {
        $prjDir = $prjDir . "/" . $prefix;
    }

    # 保证api接口准确性，多个反斜杠api调用会出问题
    my $urlLen = length($sonarUrl);
    my $endStr = substr( $sonarUrl, $urlLen - 1, $urlLen );
    if ( $endStr eq '/' ) {
        $sonarUrl = substr( $sonarUrl, 0, $urlLen - 1 );
    }

    print("INFO: Encode:$encode \n");
    print("INFO: Sonar-scanner code start, it will take a few minutes, please wait...\n");

    #下面这个两个环境变量暂时没用到
    #因为sonar-scanner/bin/sonar-scanner脚本设置了：use_embedded_jre=true
    #sonar-scanner/bin/sonar-scanner需要给java增加参数：--add-opens java.base/java.lang=ALL-UNNAMED
    $ENV{JAVA_HOME} = "$toolsPath/jdk";
    $ENV{PATH}      = "$toolsPath/jdk/bin:$toolsPath/node/bin:" . $ENV{PATH};

    $ENV{SONAR_SCANNER_OPTS} = '-Xss8m';
    my $cmd =
          "cd $prjDir;"
        . "$toolsPath/sonar-scanner/bin/sonar-scanner"
        . " -Dsonar.host.url='$sonarUrl'"
        . " -Dsonar.login='$sonarLogin'"
        . " -Dsonar.password='$sonarPwd'"
        . " -Dsonar.projectName='$projectName'"
        . " -Dsonar.language='$language'"
        . " -Dsonar.sourceEncoding='$encode'"
        . " -Dsonar.sources='$src'"
        . " -Dsonar.java.binaries='$bin'"
        . " -Dsonar.ws.timeout='$timeout'"
        . " -Dsonar.projectKey='$projectKey'"
        . " -Dsonar.projectVersion='$projectVersion'"
        . " -Dsonar.modules='$modules'";

    #. " -Dsonar.qualitygate.wait=true"
    #. " -Dsonar.qualitygate.timeout='$timeout'";

    #按时间增量代码扫描
    if ( defined($intervalDay) and $intervalDay > 0 ) {
        my $projectDate = `date -d "-$intervalDay days" '+%Y-%m-%d'`;
        chomp($projectDate);
        $cmd = $cmd . " -Dsonar.projectDate='$projectDate'";
    }

    # 扫描详情
    if ( $verbose == 1 ) {
        $cmd = $cmd . " -Dsonar.verbose=true";
    }

    my $cmdText = $cmd;
    $cmdText =~ s/$sonarPwd/******/g;

    print("INFO: Begin to execute cmd:\n");
    print("INFO: $cmdText\n");

    my $hasError = 0;
    my $ret      = system($cmd);
    $hasError = $hasError + $ret;

    if ( $ret ne 0 ) {
        print "ERROR: Sonar-scanner failed, return code:$ret.\n";
    }
    else {
        eval {
            my ( $measuresChkError, $measures ) = SonarQubeAdapter::getMeasures( $projectKey, $sonarUrl, $sonarLogin, $sonarPwd, $level, $threshold );
            $hasError = $hasError + $measuresChkError;
            if ( defined($measures) ) {
                my $serverAdapter = ServerAdapter->new();
                $serverAdapter->addBuildQuality( $buildEnv, $measures );
            }
        };
        if ($@) {
            $hasError = $hasError + 1;
            print("ERROR: $@");
        }
        else {
            print("FINE: Sonar-scanner code end .\r\n");
            print("<a target=\"_blank\" style=\"color:#a3cf62;font-size:16px;\"  href=\"$sonarUrl/dashboard?id=$projectKey\">---&gt;点击查看代码扫描报告</a>\n");
        }
    }

    return $hasError;
}

exit main();

